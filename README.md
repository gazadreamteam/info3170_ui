# INFO3170 UI Project 2 Prototype #
## Real Estate System (RES) ##
### Live Version of the Website Prototype ###

* You can view the mostly functional prototype of RES which is [freely hosted on 000webhost here.](http://info3170.000webhostapp.com/)


# Group Members #

* Odane Barnes - 620087815
* Andrew Hylton - 620088048
* Danielle Blake - 620081194
* Krystan Hunter - 620080774