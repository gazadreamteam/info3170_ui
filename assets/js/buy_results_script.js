var resultsDiv = document.getElementById("results");
var status = "";
var propertyType = "";
var cost = "";
var bedroom = "";
var bath = "";
var address = "";

var a;
var mainDiv;
var image;
var secondaryDiv;
var type;
var costText;
var prop_location;


    $.getJSON("include/buy_houses.php", function (result) {
        console.log(result);
        $.each(result, function (i, field) {
            // $("div").append(field + " ");
            status = result[i]["Available"].toLowerCase() == "buy" ? "Sale" : "Rent";
            propertyType = toTitleCase(result[i]["Propertytype"]);
            cost = result[i]["Cost"];
            bedroom = result[i]["Bedrooms"];
            bath = result[i]["Bathrooms"];
            address = toTitleCase(result[i]["Streetaddress"]);
            mainPic = result[i]["Photos"];
            querylink = result[i]["id"];
    
            
    
            aTag = document.createElement("a");
            // Add a's Href link here
    
            mainDiv = document.createElement("div");
                mainDiv.className = "col-sm-3 img-container";
    
            houseimg = document.createElement("img");
                houseimg.className = "img-responsive";
                houseimg.src = "assets/img/samplePics/" + mainPic;
    
            secondaryDiv = document.createElement("div");
                secondaryDiv.className = "img-text-bottom-left";
    
                type = document.createElement("h4");
                    type.innerText = propertyType + " For " + status;
                
                costText = document.createElement("h3");
                    if (status == "Rent") {
                        costText.innerText = cost + "/mo " + bedroom + " bed " + bath + " bath";
                    } else {
                        costText.innerText = cost + " " + bedroom + " bed " + bath + " bath";
                    }
                
                prop_location = document.createElement("h5");
                    prop_location.innerText = address;
    
            aTag.appendChild(mainDiv);
            mainDiv.appendChild(houseimg)
            mainDiv.appendChild(secondaryDiv)
    
            secondaryDiv.appendChild(type)
            secondaryDiv.appendChild(costText)
            secondaryDiv.appendChild(prop_location)
    
            resultsDiv.appendChild(aTag);
    
            // console.log(mainPic);
            // console.log(propertyType, "For", status);
            // if (status == "Rent") {
            //     console.log(cost + "/mo", bedroom, "bed", bath, "bath");
            // } else {
            //     console.log(cost, bedroom, "bed", bath, "bath");
            // }
            // console.log(address);
        });
    });
function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}