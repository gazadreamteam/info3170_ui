var resultsDiv = document.getElementById("recentlyAdded");
var buyBtn = document.getElementById("buyBtn");
var sellBtn = document.getElementById("sellBtn");
var rentBtn = document.getElementById("rentBtn");
var searchForm = document.getElementById("searchForm");
var rent_buy = document.getElementById("rent_buy");
console.log(rent_buy.value);

buyBtn.onclick = changeSearchOption;
sellBtn.onclick = changeSearchOption;
rentBtn.onclick = changeSearchOption;

var status = "";
var propertyType = "";
var cost = "";
var bedroom = "";
var bath = "";
var address = "";

var a;
var mainDiv;
var image;
var secondaryDiv;
var type;
var costText;
var prop_location;

$.getJSON("include/all_houses.php", function (result) {
    console.log(result);
    $.each(result, function (i, field) {
        // $("div").append(field + " ");
        status = result[i]["Available"].toLowerCase() == "buy" ? "Sale" : "Rent";
        propertyType = toTitleCase(result[i]["Propertytype"]);
        cost = result[i]["Cost"];
        bedroom = result[i]["Bedrooms"];
        bath = result[i]["Bathrooms"];
        address = toTitleCase(result[i]["Streetaddress"]);
        mainPic = result[i]["Photos"];
        querylink = result[i]["id"];

        

        aTag = document.createElement("a");
        // Add a's Href link here
        aTag.href = "single_view.php?id=" + querylink;

        mainDiv = document.createElement("div");
            mainDiv.className = "col-sm-3 img-container";


        houseimg = document.createElement("img");
            houseimg.className = "img-responsive";
            houseimg.src = "assets/img/samplePics/" + mainPic;

        secondaryDiv = document.createElement("div");
            secondaryDiv.className = "img-text-bottom-left";

            type = document.createElement("h4");
                type.innerText = propertyType + " For " + status;
            
            costText = document.createElement("h3");
                if (status == "Rent") {
                    costText.innerText = cost + "/mo " + bedroom + " bed " + bath + " bath";
                } else {
                    costText.innerText = cost + " " + bedroom + " bed " + bath + " bath";
                }
            
            prop_location = document.createElement("h5");
                prop_location.innerText = address;

        aTag.appendChild(mainDiv);
        mainDiv.appendChild(houseimg)
        mainDiv.appendChild(secondaryDiv)

        secondaryDiv.appendChild(type)
        secondaryDiv.appendChild(costText)
        secondaryDiv.appendChild(prop_location)

        resultsDiv.appendChild(aTag);

        // console.log(mainPic);
        // console.log(propertyType, "For", status);
        // if (status == "Rent") {
        //     console.log(cost + "/mo", bedroom, "bed", bath, "bath");
        // } else {
        //     console.log(cost, bedroom, "bed", bath, "bath");
        // }
        // console.log(address);
    });
});

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function changeSearchOption() {
    
    buyBtn.className = "col-xs-4 BRSsecondaryDivs";
    sellBtn.className = "col-xs-4 BRSsecondaryDivs";
    rentBtn.className = "col-xs-4 BRSsecondaryDivs";

    console.log(this.childNodes[1].innerText);

    buyBtn.childNodes[3].className = "arrow-down";
    sellBtn.childNodes[3].className = "arrow-down";
    rentBtn.childNodes[3].className = "arrow-down";

    this.className = "col-xs-4 BRSsecondaryDivs BRSactive";
    this.childNodes[3].className = "arrow-down arrow-down-active";

    console.log(searchForm.action);
    rent_buy.value = this.childNodes[1].innerText;
    // searchForm.action = "rent_results.php?status=" + this.childNodes[1].innerText;
}