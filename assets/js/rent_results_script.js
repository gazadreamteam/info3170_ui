var resultsDiv = document.getElementById("results");
var resultsPageHeading = document.getElementById("resultsPageHeading");
var buyNav = document.getElementById("buyNav");
var rentNav = document.getElementById("rentNav");

if (location.search.length > 0) {
    var statuslink = location.search;
} else {
    var statuslink = "?status=Rent";
}

// console.log(location.search.substr(1).split("&")[0]);
if (location.search.substr(1).split("&")[0] == "status=Rent") {
    resultsPageHeading.innerText = "Properties Available For Rent";
    document.getElementById("statusOption").innerHTML = 'For Rent <b class="caret"></b>';
    rentNav.className = "BRS-nav active";
} else {
    resultsPageHeading.innerText = "Properties Available For Sale";
    document.getElementById("statusOption").innerHTML = 'For Sale <b class="caret"></b>';
    buyNav.className = "BRS-nav active";
}


var status = "";
var propertyType = "";
var cost = "";
var bedroom = "";
var bath = "";
var address = "";

var mainlink = "";
// var statuslink = "?status=Rent";
var statuslink = location.search;
// console.log(location.search);
var bedroomlink = "bedroom>=*";
var bathroomlink = "bath>=*";

var statusOption = document.getElementById("statusOption");
    var rentBtn = document.getElementById("RentBtn");
    var saleBtn = document.getElementById("SaleBtn");
    rentBtn.onclick = updateStatus;
    saleBtn.onclick = updateStatus;

var bedroomsOption = document.getElementById("bedroomsOption");
    var zeroBedBtn = document.getElementById("zeroBedBtn");
    var oneBedBtn = document.getElementById("oneBedBtn");
    var twoBedBtn = document.getElementById("twoBedBtn");
    var threeBedBtn = document.getElementById("threeBedBtn");
    var fourBedBtn = document.getElementById("fourBedBtn");
    var fiveBedBtn = document.getElementById("fiveBedBtn");
    var sixBedBtn = document.getElementById("sixBedBtn");

    zeroBedBtn.onclick = updateBedrooms;
    oneBedBtn.onclick = updateBedrooms;
    twoBedBtn.onclick = updateBedrooms;
    threeBedBtn.onclick = updateBedrooms;
    fourBedBtn.onclick = updateBedrooms;
    fiveBedBtn.onclick = updateBedrooms;
    sixBedBtn.onclick = updateBedrooms;

var bathroomsOption = document.getElementById("bathroomsOption");
    var zeroBathBtn = document.getElementById("zeroBathBtn");
    var oneBathBtn = document.getElementById("oneBathBtn");
    var twoBathBtn = document.getElementById("twoBathBtn");
    var threeBathBtn = document.getElementById("threeBathBtn");
    var fourBathBtn = document.getElementById("fourBathBtn");
    var fiveBathBtn = document.getElementById("fiveBathBtn");
    var sixBathBtn = document.getElementById("sixBathBtn");

    zeroBathBtn.onclick = updateBathrooms;
    oneBathBtn.onclick = updateBathrooms;
    twoBathBtn.onclick = updateBathrooms;
    threeBathBtn.onclick = updateBathrooms;
    fourBathBtn.onclick = updateBathrooms;
    fiveBathBtn.onclick = updateBathrooms;
    sixBathBtn.onclick = updateBathrooms;

var a;
var mainDiv;
var image;
var secondaryDiv;
var type;
var costText;
var prop_location;

function loadResults(statuslink, bedroomlink, bathroomlink) {
    // rentLink = "include/rent_houses.php";
    // buyLink = "include/buy_houses.php";
    phpLink = "include/rent_houses.php";

    // if (statuslink.length > 0) {
    //     if (statuslink == "?status=Buy") {
    //         phpLink = buyLink + statuslink;
    //     } else {
    //         phpLink = rentLink + statuslink;
    //     }
    // }
    
    phpLink += statuslink + "&" + bedroomlink + "&" + bathroomlink;
    // window.location.href = phpLink;

    console.log(phpLink);
    $.getJSON( phpLink, function (result) {
        console.log(result);
        resultsDiv.innerHTML = "";
        $.each(result, function (i, field) {
            // $("div").append(field + " ");
            status = result[i]["Available"].toLowerCase() == "buy" ? "Sale" : "Rent";
            propertyType = toTitleCase(result[i]["Propertytype"]);
            cost = result[i]["Cost"];
            bedroom = result[i]["Bedrooms"];
            bath = result[i]["Bathrooms"];
            address = toTitleCase(result[i]["Streetaddress"]);
            mainPic = result[i]["Photos"];
            querylink = result[i]["id"];
            
    
            aTag = document.createElement("a");
            // Add a's Href link here
            aTag.href = "single_view.php?id=" + querylink;
    
            mainDiv = document.createElement("div");
                mainDiv.className = "col-sm-3 img-container";
    
            houseimg = document.createElement("img");
                houseimg.className = "img-responsive";
                houseimg.src = "assets/img/samplePics/" + mainPic;
    
            secondaryDiv = document.createElement("div");
                secondaryDiv.className = "img-text-bottom-left";
    
                type = document.createElement("h4");
                    type.innerText = propertyType + " For " + status;
                
                costText = document.createElement("h3");
                    if (status == "Rent") {
                        costText.innerText = cost + "/mo " + bedroom + " bed " + bath + " bath";
                    } else {
                        costText.innerText = cost + " " + bedroom + " bed " + bath + " bath";
                    }
                
                prop_location = document.createElement("h5");
                    prop_location.innerText = address;
    
            aTag.appendChild(mainDiv);
            mainDiv.appendChild(houseimg)
            mainDiv.appendChild(secondaryDiv)
    
            secondaryDiv.appendChild(type)
            secondaryDiv.appendChild(costText)
            secondaryDiv.appendChild(prop_location)
    
            resultsDiv.appendChild(aTag);

        });
    });
}

loadResults(statuslink, bedroomlink, bathroomlink);

function updateStatus() {
    // console.log(this.innerText);
    // console.log(this.parentNode.parentNode.parentNode.childNodes[1].innerHTML);
    // console.log(statusOption.innerHTML);
    statusOption.innerHTML = this.innerText + ' <b class="caret"></b>';
    statusOption.parentNode.className = "dropdown option-active";
    
    if (this.innerText == "For Sale") {
        queries = "Buy";
        resultsPageHeading.innerText = "Properties Available For Sale";
    } else {
        queries = "Rent";
        resultsPageHeading.innerText = "Properties Available For Rent";
    }

    statuslink = "?status=" + queries;
    loadResults(statuslink, bedroomlink, bathroomlink);
}

function updateBedrooms() {
    
    bedroomsOption.innerHTML = this.innerText[0] + '+ Beds <b class="caret"></b>';
    
    if (this.innerText[0] == "0") {
        bedroomsOption.parentNode.className = "dropdown";
    } else {
        bedroomsOption.parentNode.className = "dropdown option-active";
    }
    

    bedroomlink = "bedroom=" + this.innerText[0];
    loadResults(statuslink, bedroomlink, bathroomlink);
}

function updateBathrooms() {
    
    bathroomsOption.innerHTML = this.innerText[0] + '+ Bathrooms <b class="caret"></b>';
    
    if (this.innerText[0] == "0") {
        bathroomsOption.parentNode.className = "dropdown";
    } else {
        bathroomsOption.parentNode.className = "dropdown option-active";
    }
    

    bathroomlink = "bath=" + this.innerText[0];
    loadResults(statuslink, bedroomlink, bathroomlink);
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}



