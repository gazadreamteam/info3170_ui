    /////////////  getting elements from DOM //////////////

    var buyNav = document.getElementById("buyNav");
    var rentNav = document.getElementById("rentNav");

    var addresDiv = document.getElementById("address");
    var bedBathBeyondDiv = document.getElementById("bedBathBeyond");
    var statusDiv = document.getElementById("status");
    var costDiv = document.getElementById("cost");
    var descriptionDiv = document.getElementById("description");
    var photoSetDiv = document.getElementById("photostest");

    var resultsDiv = document.getElementById("recentlyAdded");

    var queryString = location.search;

    ////////////////////variables for data from database /////////////////////////

    var status = "";
    var propertyType = "";
    var cost = "";
    var bedroom = "";
    var bath = "";
    var address = "";
    var photo1 = "";
    var photo2 = "";
    var photo3 = "";
    var photo4 = "";
    var descrip = "";

    $.getJSON("include/single.php" + queryString, function (result) {
        console.log(result);

        $.each(result, function (i, field) {
            status = result[i]["Available"];
            propertyType = result[i]["Propertytype"];
            cost = result[i]["Cost"];
            bedroom = result[i]["Bedrooms"];
            bath = result[i]["Bathrooms"];
            address = result[i]["Streetaddress"];
            photo1 = result[i]["Photos"];
            photo2 = result[i]["Photo2"];
            photo3 = result[i]["Photo3"];
            photo4 = result[i]["Photo4"];
            descrip = result[i]["Otherinfo"];


            addresDiv.innerText = toTitleCase(address);
            bedBathBeyondDiv.innerText = bedroom + " beds " + bath + " baths 1,500 sqft";

            if (status.toLowerCase() == "rent") {
                statusDiv.innerText = "For Rent";
                costDiv.innerText = cost + " /mo";
                rentNav.className = "BRS-nav active";
            } else {
                statusDiv.innerText = "For Sale";
                costDiv.innerText = cost;
                buyNav.className = "BRS-nav active";
            }

            descriptionDiv.innerText = descrip;
            photoSetDiv.innerHTML = "";

            imgTag = document.createElement("img");
            imgTag.className = "img-responsive";
            imgTag.src = "assets/img/samplePics/" + photo1;
            photoSetDiv.appendChild(imgTag);

            imgTag2 = document.createElement("img");
            imgTag2.className = "img-responsive";
            imgTag2.src = "assets/img/samplePics/" + photo2;
            photoSetDiv.appendChild(imgTag2);

            imgTag3 = document.createElement("img");
            imgTag3.className = "img-responsive";
            imgTag3.src = "assets/img/samplePics/" + photo3;
            photoSetDiv.appendChild(imgTag3);

            imgTag4 = document.createElement("img");
            imgTag4.className = "img-responsive";
            imgTag4.src = "assets/img/samplePics/" + photo4;
            photoSetDiv.appendChild(imgTag4);

        });
    });


    $.getJSON("include/all_houses.php", function (result) {
        console.log(result);
        
        $.each(result, function (i, field) {
            
            status = result[i]["Available"].toLowerCase() == "buy" ? "Sale" : "Rent";
            propertyType = toTitleCase(result[i]["Propertytype"]);
            cost = result[i]["Cost"];
            bedroom = result[i]["Bedrooms"];
            bath = result[i]["Bathrooms"];
            address = toTitleCase(result[i]["Streetaddress"]);
            mainPic = result[i]["Photos"];
            querylink = result[i]["id"];
    
            
    
            aTag = document.createElement("a");
            // Add a's Href link here
            aTag.href = "single_view.php?id=" + querylink;
    
            mainDiv = document.createElement("div");
                mainDiv.className = "col-sm-3 img-container";
    
    
            houseimg = document.createElement("img");
                houseimg.className = "img-responsive";
                houseimg.src = "assets/img/samplePics/" + mainPic;
    
            secondaryDiv = document.createElement("div");
                secondaryDiv.className = "img-text-bottom-left";
    
                type = document.createElement("h4");
                    type.innerText = propertyType + " For " + status;
                
                costText = document.createElement("h3");
                    if (status == "Rent") {
                        costText.innerText = cost + "/mo " + bedroom + " bed " + bath + " bath";
                    } else {
                        costText.innerText = cost + " " + bedroom + " bed " + bath + " bath";
                    }
                
                prop_location = document.createElement("h5");
                    prop_location.innerText = address;
    
            aTag.appendChild(mainDiv);
            mainDiv.appendChild(houseimg)
            mainDiv.appendChild(secondaryDiv)
    
            secondaryDiv.appendChild(type)
            secondaryDiv.appendChild(costText)
            secondaryDiv.appendChild(prop_location)
    
            resultsDiv.appendChild(aTag);
        });
    });

    
function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}