<?php

$servername = "localhost";
$username = "info3170";
$password = "password";
$dbname = "info3170db";
$table_name = "house_listings";

// Create connection
$db = new mysqli($servername, $username, $password, $dbname);
$resource = $db->query('SELECT * FROM ' . $table_name);
while ($rows = $resource->fetch_assoc()) {
    //print_r($rows);
    //echo "{$row['field']}";
    // echo 'Rows Retrieved: ' . count($rows);
    $jsonData[] = $rows;
}
// echo $jsonData;
echo json_encode($jsonData);

$resource->free();
$db->close();
