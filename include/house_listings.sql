-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2017 at 06:26 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `house_listings`
--

-- --------------------------------------------------------

--
-- Table structure for table `house_listings`
--

CREATE TABLE `house_listings` (
  `id` int(10) UNSIGNED NOT NULL,
  `Streetaddress` text,
  `Available` text,
  `Bedrooms` int(11) DEFAULT NULL,
  `Bathrooms` int(11) DEFAULT NULL,
  `Propertytype` text,
  `Cost` text,
  `Otherinfo` text,
  `Photos` text,
  `Photo2` text,
  `Photo3` text,
  `Photo4` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `house_listings`
--

INSERT INTO `house_listings` (`id`, `Streetaddress`, `Available`, `Bedrooms`, `Bathrooms`, `Propertytype`, `Cost`, `Otherinfo`, `Photos`, `Photo2`, `Photo3`, `Photo4`) VALUES
(10000001, 'Kingston 8', 'Rent', 4, 5, 'House', '$1,269,100 JMD', 'interior\r\nOther Flooring: Wood, Marble, Terrazzo\r\nOther\r\nAdditional Accom.\r\nSecurity System\r\nGarden Area\r\nWater Tank\r\nWater Heater\r\nCeiling Fans\r\nGrilled\r\nKitchen Built-in(s)\r\nMain Level Entry\r\nFully Walled\r\nCable\r\nPartial Furnished\r\nSite Influences\r\nCentral location\r\nEasy Access\r\nFamily Oriented\r\nGolf Course Nearby\r\nPrivate Setting\r\nNo Thru Road\r\nLevel Lot\r\nPets Allowed', 'property1.jpg', 'img2.jpg', 'img3.jpg', 'img4.jpg'),
(10000002, 'Kingston 6', 'Rent', 4, 5, 'House', '$1,015,280 JMD', 'Interior\r\nOther Flooring: Wood, Porcelain\r\nOther\r\nSecurity System\r\nGarden Area\r\nWater Tank\r\nWater Heater\r\nCeiling Fans\r\nSwimming Pool\r\nKitchen Built-in(s)\r\nHot Tub\r\nSprinkler System\r\nFully Fenced\r\nStand-by Generator\r\nJacuzzi/Whirlpool\r\nSite Influences\r\nPrivate Setting\r\nLandscaped\r\nParking Provided\r\nDouble', 'property2.jpg', 'bikh9.jpg', 'h9h7j.jpg', 'xtdrt4.jpg'),
(10000003, 'lady kay dr. Norbrook kingston 8', 'Rent', 5, 5, 'house', '$964,516 JMD', 'Interior\r\nOther Flooring: Porcelain\r\nOther\r\nSecurity System\r\nGarden Area\r\nWater Tank\r\nWater Heater\r\nCeiling Fans\r\nSwimming Pool\r\nKitchen Built-in(s)\r\nMain Level Entry\r\nFully Fenced\r\nFurnished', 'property3.jpg', '42db121570484974fcef3e14ec7d1f68.jpg', '19ccc53d5ab140e28cea9b36bc1a1b3d.jpg', '09d049dee422c2d3c22a1507ca6c733b.jpg'),
(10000004, 'Rosa Place, Kingston6', 'Rent', 6, 4, 'house', '$824,915 JMD', 'Interior\r\nOther Flooring: Porcelain\r\nOther\r\nSecurity System\r\nGarden Area\r\nWater Tank\r\nWater Heater\r\nCeiling Fans\r\nGrilled\r\nKitchen Built-in(s)\r\nJetted Bathtub\r\nMain Level Entry\r\nWater Purifier\r\nFully Walled\r\nStand-by Generator\r\n24 Hour Security\r\nUnfurnished\r\nAppliances Only\r\nSite Influences\r\nView - Mountain\r\nCentral location\r\nEasy Access\r\nFamily Oriented\r\nPrivate Setting\r\nView - Ocean\r\nLow Maint Yard\r\nQuiet Area\r\nNo Thru Road\r\nLandscaped\r\nGated Community\r\nListed Rooms\r\nPowder Room (Downstairs)\r\nPowder Room\r\nLiving/Dining Combo\r\nFamily Room\r\nKitchen\r\nBedroom\r\nFull Bath\r\nMaster Bedroom (Upstairs)\r\nFull Master Bath (Upstairs)\r\nBedroom (Upstairs)\r\nFull Bath (Upstairs)\r\nEntrance (Other)\r\nHelper Quarters (Downstairs)\r\nLaundry/Uti (Downstairs)\r\nDen (Downstairs)', 'property4.jpg', '1d512b56a988d6438426783cc815886f.jpg', '2a23f3f0125faf07c13f89ced444faee.jpg', '07f88768f5965ec95aba2b0d3a0b9292.jpg'),
(10000005, '1 great house close, cherry gardens, Kingston', 'Rent', 5, 4, 'House', '$824,915 JMD', 'Grand living awaits at tis 8,500 square foot home with 5 bedrooms and 4 bathrooms in the heart of Cherry Gardens! It occupies 3 levels with all bedrooms on the first floor, main living rooms on the entry level floor and helpers quarter\'s, laundry and additional entertainment area on the basement level. The home is built for entertainment with a beautiful front lawn for large parties, swimming and basement level with built in bar.  Add to this loads of storage space, home office, automatic gate, 2 car enclosed automatic garage, additional carport, security system, water tank and generator and you have the full package. This home is in excellent condition with beautiful details such as the wood floors, marble floors, chandeliers and a gorgeous entrance. This is the perfect expat or company rental. Call to view today.', 'property5.jpg', '20882e2f0cfb4ec91899c0faa41366f39d2a8f84dc71b55e16ee2857a71d230c70b87ac70bd47b89e18af60b632f6ab802253a36df5a1160a08a571d0348b7de.jpg', '50f0abfd6171c31c0421b2165a18681823c2e82fe0be75f1c0619b576113ec8494b15fc662910fe547103ba3f3f6890e164a4e5caae207f41d1e311494ee3934', '5f11c8a245aa918c31d3be116f43ad052bfcd5d2549c1a8c22810e492f2f4ebf8edf78457e379450dfb4a7e4d56e0f123165aba3a9f75171ea949c15b4c7d9ff.jpg'),
(10000006, '27 dillsbury ave, jacks hill, Kingston 6', 'Rent', 3, 2, 'House', '$355,348 JMD', 'This Kingston 6 furnished, 3 bedroom, 2 bathroom townhouse is currently under renovation, but will be available for rent in mid January. Complex amenities include 24 hour security and a pool. Once renovated, inside will be like brand new. This one is worth the wait!', 'property6.jpg', '79956fbb1b7475ce1173ef939b8eaa74d5732fe7c93e10844a4a0497d324dea21ebd2acbeab3aa815347d8879a9ced2e44ad16b233318cbe98b478f9142ec144.jpg', '4aec52af9a7ffd794c0deae0a9492027ab29080d28da5c27b80eaaff248c5e87611c1d809b29df2c745ad437d793ddbe4ca48291a63a914423078d39ff42f95c.jpg', '1c0b76f70526ff5708e75eb879aaf44036cbf62e55dc169137d80f8ae1c2af75213360bbe89191a7106bc0d555de6189088bea255b8850bb175270f3071b5414.jpg'),
(10000007, 'Kingston 6', 'Rent', 2, 2, 'Apartment', '$241,129 JMD', 'The location of this cozy apartment couldn\'t be any more prime, as it is located right opposite the Bob Marley Museum on Hope Road. This 2 bedroom ground floor apartment comes equipped with all the trappings of a comfortable home and is available both for long term and short term stays at US $110/night for short term rentals. The furnished apartment has in unit laundry and amenities include 24 hour security and a pool. Call today!', 'property7.jpg', '2b335d1b10accbf2c4216369ca2ba1fd.jpg', '356ef6c0a9b9ab3a45a953fc9f888451.jpg', '773d7a182c9513624f59432a9147383d.jpg'),
(10000009, '1 hilsidedrive, Kingston', 'buy', 2, 2, 'Apartment', '$17,000,000 JMD', 'The community of Forest Hills is situated in upper St. Andrew about 17 kilometers from Kingston. The location is approximately 1400-1500 feet above sea level and is located in the Limestone Hills. The area has been classified as a upper/middle income residential suburb with numerous architecturally designed homes.The apartment complex has 24 hour security and an automatic gate.There are a total of twenty apartments spanning three blocks. The complex has a built-in swimming pool, landscaped ground and parking area. The unit comprises a master bedroom with clothes closet and full bathroom facility ensuite with standing shower and bath. The remaining bedroom has a clothes closet and full bathroom. The unit also has a designer kitchen, living/dining area, entry patio, washroom.', '5HN94RP.jpg', '8T8B4RP.jpg', '33XH4RP.jpg', 'PB554RP.jpg'),
(10000010, 'West Armour Heights, Manor Park, Kingston 8', 'Buy', 4, 4, 'House', '$35,000,000 JMD', 'This residence is located in an upscale neighbourhood above MANOR PARK. The house is 5,000 sq. ft., sits on approximately one acre with an expansive lawn and has a view of the city. It has 4 bedrooms, 3.5 bathrooms with redwood ceilings, mahogany doors, balconies and two car ports. It also has the potential to create a self-contained flat.', '8SDLWQ6.jpg', 'G8HMWQ6.jpg', 'QGSYWQ6.jpg', 'J4LBWQ6.jpg'),
(10000011, 'Jarcaranda Blvd, Old Harbour Rd, St Catherine', 'Buy', 2, 1, 'House', '$12,000,000 JMD', '2 Bedroom 1 Bathroom semi-detached House available for Sale in the gated and family oriented Jacaranda Homes on Old Harbour Road in St. Catherine. Hurry while its still available! Only JMD 12M.', 'DQ9QVAX.jpg', '6NLEVAX.jpg', 'YFS4VAX.jpg', 'LAVWVAX.jpg'),
(10000012, 'Ordon Close, Stony Hill, St Andrew', 'Buy', 3, 4, 'House', '$34,000,000 JMD', 'Gorgeous 3-bedroom 4 bathroom stand alone villa in Stony Hill with basement can be used as fourth bedroom and storeroom. In house wash area. Fully air-conditioned. Indoor private swimming pool. High ceilings. City view. Upgraded bathrooms with Jacuzzi unfurnished 300,000 USD and Furnished 340,000 USD FOR VIRTUAL TOUR http//bit.ly/2mEgvGa', 'H91BFX6.jpg', '74TLFX6.jpg', 'EPDKFX6.jpg', '6E4AFX6.jpg'),
(10000013, 'Golden Triangle, Kingston 6', 'Buy', 3, 6, 'TownHouse', '$72,000,000 JMD', 'Upscale townhouse. in a small Complex. .. centrally located in Kingston 6 ... A major hub in Kingston .. with close proximity to Sovereign Plaza, prominent High schools and Colleges . This Unit boasts a large basement with bathroom and a huge master bedroom ,with balcony .Whirlpool bath and shower in master bathroom , is part of this luxury package in this very spacious Unit In addition . there is helpers quarters and covered double car port. Complex has pool , 24 hr securityand a kiddies area as well !. Please call for appointment to view today.DL 0767', 'C2NE4VF.jpg', 'T46T4VF.jpg', 'T3DR4VF.jpg', 'T3DR4VF.jpg'),
(10000014, 'Manor Park, Kingston', 'Buy', 2, 3, 'Apartment', '$23,500,000 JMD', 'Two bedroom 23.5 . BRAND NEW .. newly constructed . Centrally located close to shopping centers,gas stations, and schools. Great value and great investment. Complex has generator, CCTV cameras pool gazebo and green courtyard. Just a few units left. D.L 076', 'B3PVA6A.jpg', '6FR9A6A.jpg', 'GJLGA6A.jpg', 'YMTAA6A.jpg'),
(10000015, 'Barbican, Kingston 6', 'Buy', 2, 3, 'Apartment', '$27,000,000JMD', 'Located in the one of the most sought after locations in Kingston 6 are these new 2 bedroom apartments. These units are 1,300 square feet, they come with two ensuite bedrooms, a powder, living/dining areas and a patio. Pictures showing completed areas represent a prior development, however, the concept is the same. Completion January 31, 2018 - Call for an appointment today.', 'JH7XM8L.jpg', 'V8USM8L.jpg', '79VDM8L.jpg', 'L2K6M8L.jpg'),
(10000016, 'Manor Park, Kingston', 'Buy', 3, 4, 'Townhouse', '$35,000,000 JMD', 'This three level well appointed townhouse is located in a beautifully landscaped Complex..Unit has spacious backyard, helpers quarters .. with 24 hour security in a prestigious neighborhood.D/L 0767', '7AQXWBY.jpg', '2PHKWBY.jpg', 'B5K7WBY.jpg', '6PMQWBY.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `house_listings`
--
ALTER TABLE `house_listings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `house_listings`
--
ALTER TABLE `house_listings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10000017;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
