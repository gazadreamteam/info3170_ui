<?php
$servername = "localhost";
$username = "info3170";
$password = "password";
$dbname = "info3170db";
$table_name = "house_listings";
$q = 'rent';

$db = new mysqli($servername, $username, $password, $dbname);

$SQL = "SELECT * FROM house_listings WHERE ";

if (isset($_GET['status'])) {
    $SQL .= "Available='" . $_GET['status'] . "' ";
}

if (isset($_GET['bedroom'])) {
    $SQL .= "AND Bedrooms>=" . $_GET['bedroom'] . " ";
}

if (isset($_GET['bath'])) {
    $SQL .= "AND Bathrooms>=" . $_GET['bath'] . " ";
}

$SQL .= "ORDER BY Bedrooms, Bathrooms ASC";

//$resource = $db->query('SELECT * FROM house_listings WHERE Available ="buy"') or die($db->error);
$resource = $db->query($SQL) or die($db->error);

$jsonData = [];
while ($rows = $resource->fetch_assoc()) {
    //print_r($rows);
    //echo "{$row['field']}";
    //echo 'Rows Retrieved: ' . count($rows);
    $jsonData[] = $rows;
}
 //echo $jsonData;
echo json_encode($jsonData);

$resource->free();
$db->close();

// $var = isset($_GET['optional']) ? $_GET['optional'] : null;

// propertyType = toTitleCase(result[i]["Propertytype"]);
// status = result[i]["Available"] == "Buy" ? "Sale" : "Rent";
// cost = result[i]["Cost"];
// bedroom = result[i]["Bedrooms"];
// bath = result[i]["Bathrooms"];
// address = toTitleCase(result[i]["Streetaddress"]);
// mainPic = result[i]["Photos"];
// querylink = result[i]["id"];

