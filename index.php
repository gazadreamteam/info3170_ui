<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RES System</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand navbar-link header-logo" href="index.php">
                    <span class="red-dash">-</span>RES<span class="red-dash">-</span>
                </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">

                <ul class="nav navbar-nav main-nav">
                    <li class="BRS-nav" role="presentation">
                        <a href="rent_results.php?status=Buy" id="buy">Buy</a>
                    </li>
                    <li class="BRS-nav" role="presentation">
                        <a href="rent_results.php?status=Rent" id="rent">Rent</a>
                    </li>
                    <li class="BRS-nav" role="presentation">
                        <a href="single_view.php">Sell</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav main-nav navbar-right">
                    
                <li role="presentation" class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            Hi John <span class="glyphicon glyphicon-user navglyph"></span> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profile_page.html">Dashboard</a>
                            </li>
                            <li>
                                <a href="#">My Wishlist</a>
                            </li>
                            <li>
                                <a href="manage_listings.html">My Properties</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li class="active">
                                <a href="#">Log Out</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <header class="img-container site-header">
        <div class="img-text-centered">
            <h1 class="indexPageTitle">Find your next home</h1>

            <div class="col-xs-12 BRSmainDivs">

                <div id="buyBtn" class="col-xs-4 BRSsecondaryDivs">
                    <div>Buy</div>
                    <div class="arrow-down"></div>
                </div>

                <div id="rentBtn" class="col-xs-4 BRSsecondaryDivs BRSactive">
                    <div>Rent</div>
                    <div class="arrow-down arrow-down-active"></div>
                </div>

                <div id="sellBtn" class="col-xs-4 BRSsecondaryDivs">
                    <div>Sell</div>
                    <div class="arrow-down"></div>
                </div>

            </div>

            <div class="col-xs-12">
                
                <form id="searchForm" action="rent_results.php" method="GET">
                    <input id="rent_buy" name="status" type="hidden" value="Rent">
                    <div class="input-group">
                        <input type="text" class="form-control" id="location" name="location" placeholder="Enter a neighbourhood, city or address">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </header>


    <div class="container-fluid section-heading">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page-title">Recently Added Properties</h1>
            </div>
        </div>
    </div>

    <div class="container-fluid after-heading">
        <div class="row">
            <div class="col-xs-12" id="recentlyAdded">

                <!-- <a href="#">
                    <div class="col-sm-3 img-container">
                        <img class="img-responsive" src="assets/img/camera_illustration.jpg">

                        <div class="img-text-bottom-left">
                            <h4>House For Rent</h4>
                            <h3>$40,000/mo 3 bed 2 bath</h3>
                            <h5>Kingston</h5>
                        </div>
                    </div>
                </a> -->
                

            </div>

        </div>
    </div>

    <footer>
        <h5 class="text-center">RES System © 2017</h5>
    </footer>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/home_script.js"></script>
</body>

</html>