<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RES System</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand navbar-link header-logo" href="index.php">
                    <span class="red-dash">-</span>RES<span class="red-dash">-</span>
                </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">

                <ul class="nav navbar-nav main-nav">
                    <li id="buyNav" class="BRS-nav" role="presentation">
                        <a href="rent_results.php?status=Buy">Buy</a>
                    </li>
                    <li id="rentNav" class="BRS-nav" role="presentation">
                        <a href="rent_results.php?status=Rent">Rent</a>
                    </li>
                    <li class="BRS-nav" role="presentation">
                        <a href="#">Sell</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav main-nav navbar-right">
                    <li role="presentation" class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            Hi John <span class="glyphicon glyphicon-user navglyph"></span> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profile_page.html">Dashboard</a>
                            </li>
                            <li>
                                <a href="#">My Wishlist</a>
                            </li>
                            <li>
                                <a href="manage_listings.html">My Properties</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li class="active">
                                <a href="#">Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>


            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <li class="dropdown option-active">
                        <a id="statusOption" href="#" class="dropdown-toggle" data-toggle="dropdown">For Rent <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div id="RentBtn" class="btn btn-block">For Rent</div>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <div id="SaleBtn" class="btn btn-block">For Sale</div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Any Price
                            <b class="caret"></b>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <div class="form-group">

                                    <div class="col-xs-12 priceControls">
                                        <div class="col-xs-12">
                                            <input class="form-control" id="ex1" type="text">
                                        </div>
                                        
                                        <div class="col-xs-12">
                                            <input class="form-control" id="ex2" type="text">
                                        </div>
                                    </div>

                                </div>
                            </li>

                            <li>
                                <a href="#">Any Price</a>
                            </li>
                            <li class="divider"></li>

                        </ul>
                    </li>

                    <li class="dropdown">
                        <a id="bedroomsOption" href="#" class="dropdown-toggle" data-toggle="dropdown">0+ Beds <b class="caret"></b></a>

                        <ul class="dropdown-menu">
                            <li>
                                <div id="zeroBedBtn" class="btn btn-block">0+</div>
                            </li>
                            <li>
                                <div id="oneBedBtn" class="btn btn-block">1+</div>
                            </li>
                            <li>
                                <div id="twoBedBtn" class="btn btn-block">2+</div>
                            </li>
                            <li>
                                <div id="threeBedBtn" class="btn btn-block">3+</div>
                            </li>
                            <li>
                                <div id="fourBedBtn" class="btn btn-block">4+</div>
                            </li>
                            <li>
                                <div id="fiveBedBtn" class="btn btn-block">5+</div>
                            </li>
                            <li>
                                <div id="sixBedBtn" class="btn btn-block">6+</div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a id="bathroomsOption" href="#" class="dropdown-toggle" data-toggle="dropdown">0+ Bathrooms <b class="caret"></b></a>

                        <ul class="dropdown-menu">
                            <li>
                                <div id="zeroBathBtn" class="btn btn-block">0+</div>
                            </li>
                            <li>
                                <div id="oneBathBtn" class="btn btn-block">1+</div>
                            </li>
                            <li>
                                <div id="twoBathBtn" class="btn btn-block">2+</div>
                            </li>
                            <li>
                                <div id="threeBathBtn" class="btn btn-block">3+</div>
                            </li>
                            <li>
                                <div id="fourBathBtn" class="btn btn-block">4+</div>
                            </li>
                            <li>
                                <div id="fiveBathBtn" class="btn btn-block">5+</div>
                            </li>
                            <li>
                                <div id="sixBathBtn" class="btn btn-block">6+</div>
                            </li>
                        </ul>
                    </li>

                </ul>

                <div class="col-xs-12 col-sm-4 col-md-6">
                    <form class="navbar-form" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="q">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            
        </div>
    </nav>

    <div class="container-fluid section-heading">
        <div class="row">
            <div class="col-xs-12">
                <h3 id="resultsPageHeading" class="">Properties Available For Rent</h3>

                <div class="col-xs-12 col-sm-6 sortOptions">
                    <div class="singleOption option-active">Newest</div>
                    <div class="singleOption">Rent (low to high)</div>
                    <div class="singleOption">More ▼</div>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid after-heading">
        <div class="row">
            <div class="col-xs-12" id = "results">

                
        </div>
    </div>
   
    <footer>
        <h5 class="text-center">RES System © 2017</h5>
    </footer>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/rent_results_script.js"></script>
</body>

</html>