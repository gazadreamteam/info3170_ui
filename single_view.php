<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RES System</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>

    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand navbar-link header-logo" href="index.php">
                    <span class="red-dash">-</span>RES<span class="red-dash">-</span>
                </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">

                <ul class="nav navbar-nav main-nav">
                    <li id="buyNav" class="BRS-nav" role="presentation">
                        <a href="rent_results.php?status=Buy">Buy</a>
                    </li>
                    <li id="rentNav" class="BRS-nav" role="presentation">
                        <a href="rent_results.php?status=Buy">Rent</a>
                    </li>
                    <li class="BRS-nav" role="presentation">
                        <a href="#">Sell</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav main-nav navbar-right">
                    <li role="presentation" class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            Hi John <span class="glyphicon glyphicon-user navglyph"></span> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profile_page.html">Dashboard</a>
                            </li>
                            <li>
                                <a href="#">My Wishlist</a>
                            </li>
                            <li>
                                <a href="manage_listings.html">My Properties</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li class="active">
                                <a href="#">Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid section-heading">
        <div class="row">
            <div class="col-xs-12 col-sm-6">

                <div class="col-xs-12 col-sm-8 propDesc">
                    <h2 id="address" class="">Mona Heights<br>
                                 Kingston 7, CSO
                    </h2>
                    <!-- <h2 class="">Kingston 7, CSO</h2> -->
                    <h4 id="bedBathBeyond" class="">6 beds 3 baths 3,300 sqft</h4>
                </div>

                <div class="col-xs-12 col-sm-4 propDesc">
                    <h5 id="status" class="">For Rent</h5>
                    <h4 id="cost" class="">$40,000 /mo</h4>
                </div>

                <div class="col-xs-12">
                    <p id="description">
                       Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque ad similique 
                       nulla nemo. Voluptatem, voluptates consequatur sed earum aspernatur, eum 
                       quisquam quo quis ipsum, at optio expedita minima nostrum incidunt voluptatibus 
                       quasi. Optio, recusandae libero necessitatibus veniam rem inventore blanditiis 
                       laboriosam fugiat atque repudiandae tempore sunt doloremque at.
                    </p>
                    <br>
                </div>
                
                <div class="col-xs-12 singlePageButtons">
                    <button class="btn btn-info">Reserve</button>
                    <button class="btn btn-success">Add to Watchlist</button>
                </div>

            </div>

            <div class="col-xs-12 col-sm-6">
                <section id="photostest">
                    <!-- <img class="img-responsive" src="assets/img/camera_illustration.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover-2.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover-3.jpeg">
                    <img class="img-responsive" src="assets/img/photography_background.jpg">
                    <img class="img-responsive" src="assets/img/camera_illustration.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover-2.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover-3.jpeg">
                    <img class="img-responsive" src="assets/img/photography_background.jpg">
                    <img class="img-responsive" src="assets/img/camera_illustration.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover-2.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover-3.jpeg">
                    <img class="img-responsive" src="assets/img/photography_background.jpg">
                    <img class="img-responsive" src="assets/img/camera_illustration.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover-2.jpg">
                    <img class="img-responsive" src="assets/img/homepage-cover-3.jpeg">
                    <img class="img-responsive" src="assets/img/photography_background.jpg"> -->
                </section>

            </div>
        </div>
    </div>

    <div class="container-fluid section-heading">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page-title">Other Similar Properties</h1>
            </div>
        </div>
    </div>

    <div class="container-fluid after-heading">
        <div class="row">
            <div id="recentlyAdded" class="col-xs-12">

            </div>

        </div>
    </div>

    <footer>
        <h5 class="text-center">RES System © 2017</h5>
    </footer>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/single.js"></script>
</body>

</html>